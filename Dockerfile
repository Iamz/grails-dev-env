FROM openjdk:11

COPY . /app

WORKDIR /app

EXPOSE 8080

CMD ["bash", "./gradlew", "bootRun"]
